/* Local customisation should be added to this file and linked/coppied
 * in the build directory.  Options defined here will override any
 * options in default_options.h.  See upstream's INSTALL file. */

/* Enable running an OpenSSH's sftp server (the sftp server program is
 * not provided by Dropbear itself) */
#define DROPBEAR_SFTPSERVER 1
#define SFTPSERVER_PATH "/usr/lib/sftp-server"

/* The default PATH environment variable when a regular user resp. the
 * superuser logs in.  See login.defs(5) and https://bugs.debian.org/903403 */
#define DEFAULT_PATH "/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games"
#define DEFAULT_ROOT_PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
